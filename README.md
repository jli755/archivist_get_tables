### heroku commands

- `heroku login`
- `heroku apps`
- `heroku psql -a closer-temp`

### use the script

1. modify the prefix inside `get_tables.sql`
   - the prefix is defined using `\set myPrefix`

2. in command line, type
   ```
   heroku psql -a closer-temp < get_tables.sql
   ```

