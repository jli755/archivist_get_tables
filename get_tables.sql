
\set myPrefix '\'' 'heaf_17_fup4' '\''

-- 1. sequences;
\qecho '1. sequences';

drop table temp_sequence;

create table temp_sequence as
select s.label as "Label",
       s.parent_type as "Parent_Type",
       ss.label as "Parent_Name",
       s.branch as "Branch",
       s.position as "Position"
from cc_sequences s
join instruments i on i.id = s.instrument_id
join cc_sequences ss on ss.id = s.parent_id
where i.prefix = :myPrefix
and s.parent_type is not null
order by s.parent_id, s.position;

\copy (select * from temp_sequence) To 'sequence.csv' With CSV DELIMITER E'\t' HEADER;


-- 2. response domain;
\qecho '2. response domain';

drop table temp_response_domain;

create table temp_response_domain as
select rdn.id as r_id,
       rdn.label as "Label",
       'Numeric' as "Type",
       rdn.numeric_type as "Type2",
       null as "Format",
       rdn.min as "Min",
       rdn.max as "Max"
from response_domain_numerics rdn
join instruments i on i.id = rdn.instrument_id
where i.prefix = :myPrefix
union
select rdt.id as r_id,
       rdt.label as "Label",
       'Text' as "Type",
       null as "Type2",
       null as "Format",
       null as "Min",
       rdt.maxlen as "Max"
from response_domain_texts rdt
join instruments i on i.id = rdt.instrument_id
where i.prefix = :myPrefix
union
select rdd.id as r_id,
       rdd.label as "Label",
       'Date' as "Type",
       rdd.datetime_type as "Type2",
       rdd.format as "Format",
       null as "Min",
       null as "Max"
from response_domain_datetimes rdd
join instruments i on i.id = rdd.instrument_id
where i.prefix = :myPrefix
;

\copy (select "Label", "Type", "Type2", "Format", "Min", "Max" from temp_response_domain) To 'response.csv' With CSV DELIMITER E'\t' HEADER;


-- 3. code list;
\qecho '3. code list';

drop table temp_codelist;

create table temp_codelist as
select rdc.id as r_id,
       rdc.min_responses,
       rdc.max_responses,
       cl.label as "Label",
       c.order as "Code_Order",
       c.value as "Code_Value",
       ca.label as "Category"
from response_domain_codes rdc
join instruments i on i.id = rdc.instrument_id
left join code_lists cl on cl.id = rdc.code_list_id
left join codes c on c.code_list_id = cl.id
left join categories ca on ca.id = c.category_id
where i.prefix = :myPrefix ;

\copy (select "Label", "Code_Order", "Code_Value", "Category" from temp_codelist) To 'codelist.csv' With CSV DELIMITER E'\t' HEADER;


-- 4. question items;
\qecho '4. question items';

drop table temp_question_item;

create table temp_question_item as
select distinct
       qi.id,
       qi.label as "Label",
       qi.literal as "Literal",
       it.text as "Instructions",
       COALESCE(nc."Label", nr."Label")  as "Response",
       ccq.parent_id,
       ccq.parent_type as "Parent_Type",
       COALESCE(ccs.Label, ccc.Label, ccl.Label) as "Parent_Name",
       ccq.branch as "Branch",
       ccq.position as "Position",
       nc.min_responses,
       nc.max_responses,
       rds_qs.rd_order,
       ru.label as "Interviewee"
from question_items qi
join instruments i on i.id = qi.instrument_id
left join instructions it on qi.instruction_id = it.id
      and i.id = it.instrument_id
left join cc_questions ccq on ccq.question_id = qi.id
left join rds_qs on rds_qs.question_id = qi.id
      and rds_qs.instrument_id = i.id
left join temp_codelist nc on nc.r_id = rds_qs.response_domain_id
left join temp_response_domain nr on nr.r_id = rds_qs.response_domain_id
left join cc_sequences ccs on ccs.id = ccq.parent_id
left join cc_conditions ccc on ccc.id = ccq.parent_id
left join cc_loops ccl on ccl.id = ccq.parent_id
left join response_units ru on i.id = ru.instrument_id and ccq.response_unit_id = ru.id
where i.prefix = :myPrefix
order by qi.id ;

\copy (select "Label", "Literal", "Instructions", "Response", "Parent_Type", "Parent_Name", "Branch", "Position", "min_responses", "max_responses", "rd_order", "Interviewee" from temp_question_item) To 'question_item.csv' With CSV DELIMITER E'\t' HEADER;


-- 5. question grid;
\qecho '5. question grid';

drop table temp_question_grid;

create table temp_question_grid as
select distinct
       qg.label as "Label",
       qg.literal as "Literal",
       it.text as "Instructions",
       clh.Label as "Horizontal_Codelist_Name",
       clv.Label as "Vertical_Codelist_Name",
       COALESCE(nc."Label", nr."Label")  as "Response_domain",
       ccq.parent_type as "Parent_Type",
       COALESCE(ccs.Label, ccc.Label, ccl.Label) as "Parent_Name",
       ccq.branch as "Branch",
       ccq.position as "Position",
       nclh.min_responses as "Horizontal_min_responses",
       nclh.max_responses as "Horizontal_max_responses",
       nclv.min_responses as "Vertical_min_responses",
       nclv.max_responses as "Vertical_max_responses",
       ru.label as "Interviewee"
from question_grids qg
join instruments i on i.id = qg.instrument_id
left join instructions it on qg.instruction_id = it.id
      and i.id = it.instrument_id
left join code_lists clv on clv.id = qg.vertical_code_list_id
left join code_lists clh on clh.id = qg.horizontal_code_list_id
left join temp_codelist nclv on nclv."Label" =  clv.label
left join temp_codelist nclh on nclh."Label" =  clh.label
left join cc_questions ccq on ccq.question_id = qg.id
left join rds_qs on rds_qs.question_id = qg.id
      and rds_qs.instrument_id = i.id
left join temp_codelist nc on nc.r_id = rds_qs.response_domain_id
left join temp_response_domain nr on nr.r_id = rds_qs.response_domain_id
left join cc_sequences ccs on ccs.id = ccq.parent_id
left join cc_conditions ccc on ccc.id = ccq.parent_id
left join cc_loops ccl on ccl.id = ccq.parent_id
left join response_units ru on i.id = ru.instrument_id and ccq.response_unit_id = ru.id
where i.prefix = :myPrefix;

\copy (select * from temp_question_grid) To 'question_grid.csv' With CSV DELIMITER E'\t' HEADER;


-- 6. statement;
\qecho '6. statement';

drop table temp_statement;

create table temp_statement as
select ccs.Label as "Label",
       ccs.Literal as "Literal",
       ccs.Parent_type as "Parent_Type",
       COALESCE(ccse.Label, ccc.Label, ccl.Label) as "Parent_Name",
       ccs.Branch as "Branch",
       ccs.Position as "Position"
from cc_statements ccs
join instruments i on i.id = ccs.instrument_id
left join cc_sequences ccse on ccse.id = ccs.parent_id
left join cc_conditions ccc on ccc.id = ccs.parent_id
left join cc_loops ccl on ccl.id = ccs.parent_id
where i.prefix = :myPrefix;

\copy (select * from temp_statement) To 'statement.csv' With CSV DELIMITER E'\t' HEADER;


-- 7. condition;
\qecho '7. condition';

drop table temp_condition;

create table temp_condition as
select ccc.label as "Label",
       ccc.literal as "Literal",
       ccc.logic as "Logic",
       ccc.parent_type as "Parent_Type",
       COALESCE(ccse.Label, ccs.Label, ccl.Label, ccc_1.Label) as "Parent_Name",
       ccc.branch as "Branch",
       ccc.position as "Position"
from cc_conditions ccc
join instruments i on i.id = ccc.instrument_id
left join cc_sequences ccse on ccse.id = ccc.parent_id
left join cc_statements ccs on ccc.id = ccs.parent_id
left join cc_loops ccl on ccl.id = ccc.parent_id
left join cc_conditions ccc_1 on ccc_1.id = ccc.parent_id
where i.prefix = :myPrefix;

\copy (select * from temp_condition) To 'condition.csv' With CSV DELIMITER E'\t' HEADER;


-- 8. loops;
\qecho '8. loops';

drop table temp_loop;

create table temp_loop as
select ccl.label as "Label",
       ccl.loop_while as "Loop_While",
       ccl.start_val as "Start_Value",
       ccl.end_val as "End_Value",
       ccl.loop_var as "Variable",
       ccl.parent_type as "Parent_Type",
       COALESCE(ccse.Label, ccs.Label, ccc.Label) as "Parent_Name",
       ccl.branch as "Branch",
       ccl.position as "Position"
from cc_loops ccl
join instruments i on i.id = ccl.instrument_id
left join cc_sequences ccse on ccse.id = ccl.parent_id
left join cc_statements ccs on ccs.id = ccl.parent_id
left join cc_conditions ccc on ccc.id = ccl.parent_id
where i.prefix = :myPrefix;

\copy (select * from temp_loop) To 'loop.csv' With CSV DELIMITER E'\t' HEADER;

drop table temp_sequence;
drop table temp_response_domain;
drop table temp_codelist;
drop table temp_question_item;
drop table temp_question_grid;
drop table temp_statement;
drop table temp_condition;
drop table temp_loop;
